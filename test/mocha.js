const expect = require('chai').expect;
const faker = require('faker');
const config = require('../config');
const helper = require('../helper');

describe('Mocha', () => {
    describe('Check adding products', () => {
        it('To cart', async () => {
            const data = {
                productPageUrl: config.websiteUrl + 'zinzi-classy-horloge-34mm-donkerrode-wijzerplaat-rosegoudkleurige-stalen-kast-en-bicolor-band-datum-ziw1038',
                cartPageUrl: config.websiteUrl + 'checkout/cart/',
                productQuantity: '10'
            };

            const {driver, By, util} = helper.getDriver();

            await driver.get(data.productPageUrl);

            let quantityInput = await helper.findElementById('qty', driver);
            await quantityInput.clear();
            await quantityInput.sendKeys(data.productQuantity);

            let addProductButton = await helper.findElementById('product-addtocart-button', driver);
            await addProductButton.click();

            await driver.sleep(config.pageLoadTime);

            let counterElement = await helper.findElementByClass('counter-number', driver);
            let counterValue = await counterElement.getText();

            let urlString = await driver.getCurrentUrl();

            await driver.quit();

            expect(urlString).to.equal(data.cartPageUrl);
            expect(counterValue).to.equal(data.productQuantity);
        });
    });

    describe('Check address editing', () => {
        it('With invalid data', async () => {
            const data = {
                loginPageUrl: config.websiteUrl + 'customer/account/login/',
                addressBookUrl: config.websiteUrl + 'customer/address/index/',
                addressEditPageUrl: config.websiteUrl + 'customer/address/edit/'
            };

            const {driver, By, util} = helper.getDriver();

            await driver.get(data.loginPageUrl);

            let emailInput = await helper.findElementById('email', driver);
            emailInput.sendKeys(config.email);

            let passwordInput = await helper.findElementById('pass', driver);
            passwordInput.sendKeys(config.password);

            let loginButton = await helper.findElementByClass('action login primary', driver);
            await loginButton.click();

            await driver.get(data.addressBookUrl);

            let addressSection = await helper.findElementByClass('box box-address-billing', driver);
            let editAddressButton = await addressSection.findElements(By.className('action edit'));
            await editAddressButton.click();

            await driver.sleep(config.pageLoadTime);

            let phoneNumberInput = await helper.findElementById('telephone', driver);
            await phoneNumberInput.clear();

            let saveButton = await helper.findElementByClass('action save primary', driver);
            await saveButton.click();

            let urlString = await driver.getCurrentUrl();

            await driver.quit();

            expect(urlString.includes(data.addressEditPageUrl)).to.equal(true);
        });

        it('With valid data', async () => {
            const data = {
                loginPageUrl: config.websiteUrl + 'customer/account/login/',
                addressBookUrl: config.websiteUrl + 'customer/address/index/',
                phoneNumber: faker.phone.phoneNumber('###-###-####')
            };

            const {driver, By, util} = helper.getDriver();

            await driver.get(data.loginPageUrl);

            let emailInput = await helper.findElementById('email', driver);
            emailInput.sendKeys(config.email);

            let passwordInput = await helper.findElementById('pass', driver);
            passwordInput.sendKeys(config.password);

            let loginButton = await helper.findElementByClass('action login primary', driver);
            await loginButton.click();

            await driver.get(data.addressBookUrl);

            let addressSection = await helper.findElementByClass('box box-address-billing', driver);
            let editAddressButton = (await addressSection.findElements(By.className('action edit')))[0];
            await editAddressButton.click();

            await driver.sleep(config.pageLoadTime);

            let phoneNumberInput = await helper.findElementById('telephone', driver);
            await phoneNumberInput.clear();
            await phoneNumberInput.sendKeys(data.phoneNumber);

            let saveButton = await helper.findElementByClass('action save primary', driver);
            await saveButton.click();

            let urlString = await driver.getCurrentUrl();

            await driver.quit();

            expect(urlString).to.equal(data.addressBookUrl);
        });
    });

    describe('Check login', () => {
        it('With invalid credentials', async () => {
            const data = {
                loginPageUrl: config.websiteUrl + 'customer/account/login/',
                fakePassword: faker.internet.password(8)
            };

            const {driver, By, util} = helper.getDriver();

            await driver.get(data.loginPageUrl);

            let emailInput = await helper.findElementById('email', driver);
            emailInput.sendKeys(config.email);

            let passwordInput = await helper.findElementById('pass', driver);
            passwordInput.sendKeys(data.fakePassword);

            let loginButton = await helper.findElementByClass('action login primary', driver);
            await loginButton.click();

            let urlString = await driver.getCurrentUrl();

            await driver.quit();

            expect(urlString).to.equal(data.loginPageUrl);
        });

        it('With valid credentials', async () => {
            const data = {
                loginPageUrl: config.websiteUrl + 'customer/account/login/',
                accountPageUrl: config.websiteUrl + 'customer/account/'
            };

            const {driver, By, util} = helper.getDriver();

            await driver.get(data.loginPageUrl);

            let emailInput = await helper.findElementById('email', driver);
            emailInput.sendKeys(config.email);

            let passwordInput = await helper.findElementById('pass', driver);
            passwordInput.sendKeys(config.password);

            let loginButton = await helper.findElementByClass('action login primary', driver);
            await loginButton.click();

            let urlString = await driver.getCurrentUrl();

            await driver.quit();

            expect(urlString).to.equal(data.accountPageUrl);
        });
    });

    describe('Check page', () => {
        it('Of logout', async () => {
            const data = {
                loginPageUrl: config.websiteUrl + 'customer/account/login/',
                logoutControllerUrl: config.websiteUrl + 'customer/account/logout/',
                logoutSuccessPageUrl: config.websiteUrl + 'customer/account/logoutSuccess/'
            };

            const {driver, By, util} = helper.getDriver();

            await driver.get(data.loginPageUrl);

            let emailInput = await helper.findElementById('email', driver);
            emailInput.sendKeys(config.email);

            let passwordInput = await helper.findElementById('pass', driver);
            passwordInput.sendKeys(config.password);

            let loginButton = await helper.findElementByClass('action login primary', driver);
            await loginButton.click();

            await driver.get(data.logoutControllerUrl);

            let urlString = await driver.getCurrentUrl();

            await driver.quit();

            expect(urlString).to.equal(data.logoutSuccessPageUrl);
        });

        it('Of minicart', async () => {
            const data = {};

            const {driver, By, util} = helper.getDriver();

            await driver.get(config.websiteUrl);

            await driver.sleep(config.pageLoadTime * 2);

            let minicartButton = await helper.findElementByClass('action showcart', driver);
            let minicartElement = await helper.findElementById('ui-id-1', driver);

            await minicartButton.click();
            let isMinicartAppear = await minicartElement.isDisplayed();

            await minicartButton.click();
            let isMinicartDisappear = !(await minicartElement.isDisplayed());

            await driver.quit();

            expect(isMinicartAppear && isMinicartDisappear).to.equal(true);
        });

        it('Of search', async () => {
            const data = {
                searchText: 'Ring'
            };

            const {driver, By, util} = helper.getDriver();

            await driver.get(config.websiteUrl);

            let searchInput = await helper.findElementById('search', driver);
            await searchInput.sendKeys(data.searchText);

            await driver.sleep(config.pageLoadTime);

            let searchButton = await helper.findElementByClass('action search', driver);
            await searchButton.click();

            let urlString = await driver.getCurrentUrl();
            let url = new URL(urlString);
            let urlSearchText = url.searchParams.get('q');

            await driver.quit();

            expect(urlSearchText).to.equal(data.searchText);
        });
    });

    describe('Check removing products', () => {
        it('From cart', async () => {
            const data = {
                productPageUrl: config.websiteUrl + 'zinzi-classy-horloge-34mm-donkerrode-wijzerplaat-rosegoudkleurige-stalen-kast-en-bicolor-band-datum-ziw1038',
                cartPageUrl: config.websiteUrl + 'checkout/cart/',
                productQuantity: '0'
            };

            const {driver, By, util} = helper.getDriver();

            await driver.get(data.productPageUrl);

            let addProductButton = await helper.findElementById('product-addtocart-button', driver);
            await addProductButton.click();

            await driver.sleep(config.pageLoadTime);

            let removeProductButton = await helper.findElementByClass('action action-delete', driver);
            await removeProductButton.click();

            await driver.sleep(config.pageLoadTime);

            let counterElement = await helper.findElementByClass('counter-number', driver);
            let counterValue = await counterElement.getText();

            let urlString = await driver.getCurrentUrl();

            await driver.quit();

            expect(urlString).to.equal(data.cartPageUrl);
            expect(counterValue).to.equal(data.productQuantity);
        });

        it('From minicart', async () => {
            const data = {
                productPageUrl: config.websiteUrl + 'zinzi-classy-horloge-34mm-donkerrode-wijzerplaat-rosegoudkleurige-stalen-kast-en-bicolor-band-datum-ziw1038',
                productQuantity: '0'
            };

            const {driver, By, util} = helper.getDriver();

            await driver.get(data.productPageUrl);

            let addProductButton = await helper.findElementById('product-addtocart-button', driver);
            await addProductButton.click();

            await driver.sleep(config.pageLoadTime);

            let minicartButton = await helper.findElementByClass('action showcart', driver);
            await minicartButton.click();

            let removeProductButton = await helper.findElementByClass('action delete', driver);
            await removeProductButton.click();

            await driver.sleep(config.pageLoadTime);

            let confirmButton = await helper.findElementByClass('action-primary action-accept', driver);
            await confirmButton.click();

            await driver.sleep(config.pageLoadTime);

            let counterElement = await helper.findElementByClass('counter-number', driver);
            let counterValue = await counterElement.getText();

            await driver.quit();

            expect(counterValue).to.equal(data.productQuantity);
        });
    });
});
