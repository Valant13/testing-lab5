let helper = {
    getDriver: function() {
        const webdriver = require('selenium-webdriver'),
            By = webdriver.By,
            until = webdriver.until;

        const { Builder, Capabilities } = require('selenium-webdriver');
        let capabilities = Capabilities.chrome();
        let driver = new Builder()
            .usingServer('http://localhost:4444/wd/hub')
            .withCapabilities(capabilities)
            .build();

        return {driver, By, until};
    },

    getBy: function() {
        return require('selenium-webdriver').By;
    },

    findElementById: async function(id, driver) {
        const By = this.getBy();

        return await driver.findElement(By.id(id));
    },

    findElementByClass: async function(className, driver) {
        const By = this.getBy();

        return (await driver.findElements(By.className(className)))[0];
    }
};

module.exports = helper;
